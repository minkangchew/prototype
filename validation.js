// Validation Schema
const Joi = require("joi");

// Input Validation
const inputValidation = data => {
  const schema = Joi.object({
    tm: Joi.string().required(),
    status: Joi.string().required(),
    uploader: Joi.string().required(),
    reviewer: Joi.string().required()
  });
  return schema.validate(data);
};

module.exports.inputValidation = inputValidation;
