const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv/config");

const app = express();
app.use(express.json());
app.use(cors());
app.set("json spaces", 2);

// Import routes
const documentRoute = require("./routes/document");

// Middleware
app.use("/api", documentRoute);

// Home route
app.get("/", (req, res) => {
  res.send("we are at home");
});

// Connect to MongoDB
mongoose.connect(process.env.DB_CONNECTION_LOCAL, () =>
  console.log("connected to DB")
);

// listen to PORT
port = process.env.PORT || 3000;
app.listen(port);
